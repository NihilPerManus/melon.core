# Melon.Core?!

Melon.core is a base set of Classes and interfaces that I find myself rewriting constantly.
I've intended to use this in all my projects and will eventually fork


## Installation
From the package manager console simply type **install-package melon.core**

Everything within this package falls within the *Melon.Core* namespace.

### Useful classes/Interfaces ###
- ObservableClass - Provides a class with the NotifyPropertyChanged() method and handles the INotifyPropertyChanged interface
- SecureString/StringExtensions - Provides a means of converting to and from securestring for authentication based tasks
- RelayCommand - Provides an implementation of the ICommand class used for WPF based event triggers
- IRepository - Interfaces to assist with shaping a class to the repository pattern. There are numerous types within the namespace to allow for fine-grained control
- CQRS - Interfaces which provide a solid way of developing in CQRS
- IContainer<T> - Provides an interface for storing a value inside a container for ease of passing around values within a singleton. The Provided baseclass simplifies a single-container class