﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;

namespace Melon.Core.Extensions
{
    public static class StringExtensions
    {
        public static SecureString ToSecureString(this string me)
        {
            SecureString secure = new SecureString();

            me.ToList()?
                .ForEach(secure.AppendChar);

            return secure;
        }
    }
}
