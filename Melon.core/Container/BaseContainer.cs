﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Melon.Core.Container
{
    public class BaseContainer<T> : Melon.Core.Observable.ObservableClass, IContainer<T>
    {
        private T __Value;
        public virtual T Value { get { return __Value; } set { __Value = value; Console.WriteLine($"Notified change of [{typeof(T)}] ({__Value})"); NotifyPropertyChanged();  } }
    }
}
