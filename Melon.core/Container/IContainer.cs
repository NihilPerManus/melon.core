﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Melon.Core.Container
{ 
    //Containers are using to provide objects in a package for singleton consumption. 
    //it is likey in some scenarios that a class, or a collection of classes can't be made singleton and shared easily.
    //storing in a container class and making that a singlton is a workaround for now.
    //It also allows for multiple classes to be notified when that value changes.

    //Also helps pass singleton primitives around.
    public interface IContainer<T> : INotifyPropertyChanged
    {
        T Value { get; set; }
    }
}
