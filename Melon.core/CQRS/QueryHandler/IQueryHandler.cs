﻿using Melon.Core.CQRS.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Melon.Core.CQRS.QueryHandler
{
    public interface IQueryHandler<TQuery, TReturn> where TQuery : IQuery
    {
        TReturn Handle(TQuery query);
    }
    public interface IQueryHandler<TReturn>
    {
        TReturn Handle();
    }
}
