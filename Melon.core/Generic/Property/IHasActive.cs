﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Melon.Core.Generic.Property
{
    public interface IHasActive
    {
        bool IsActive { get; set; }
    }
}
