﻿using Melon.Core.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Melon.Core.Cache
{
    public abstract class TimeBasedCache<TClass, TUnique> : ITimeBasedCache<TClass,TUnique>
        where TClass : class
    {
        List<TClass> _cache = new List<TClass>();
        Dictionary<TUnique, DateTime> _ttl = new Dictionary<TUnique, DateTime>();

        TimeSpan _Retention = new TimeSpan();
        
        public TimeSpan Retention { get => _Retention; set => _Retention = value; }

        public abstract void Delete(TClass id);

        public abstract TClass Get(TUnique id);

        public abstract IEnumerable<TClass> GetAll(TUnique id);

        public abstract void Set(TClass entity);

        public abstract void SetRange(IEnumerable<TClass> entities);

        protected bool IsAlive(TUnique id)
        {
            var expiryDate = _ttl[id].Add(Retention);

            if (DateTime.Now > expiryDate)
                return false;
            return true;
        }

    }

    public interface ITimeBasedCache<TClass, TUnique> : ICache<TClass, TUnique>
        where TClass : class
    {        
        TimeSpan Retention { get; set; } //How long will it hold onto data for?
    }
}
