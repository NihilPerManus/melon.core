﻿using Melon.Core.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Melon.Core.Cache
{
    public interface ICache<TClass, TUnique> : IRepository<TClass,TUnique>
        where TClass : class
    {
        //Cache is pretty much a repository only it should be used in parallel.
        //How you handle caching is up to you. A good use would be to add a lifespan so that you can get a new value after a period of time has elapsed
        //If that's the case you could hace the cache sit between your app and the repository

        /*
         

        [ YOUR APP ] < = > [ CACHE ] < = > [ REPO ]

         */

    }
}
