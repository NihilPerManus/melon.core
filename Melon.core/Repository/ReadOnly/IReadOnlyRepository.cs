﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Melon.Core.Repository.ReadOnly
{
    public interface IReadOnlyRepository<TClass, TUnique> : IGetRepository<TClass,TUnique>, IGetAllRepository<TClass,TUnique> 
        where TClass : class
    {
        
    }

    public interface IReadOnlyRepositoryAsync<TClass, TUnique> : IGetRepositoryAsync<TClass, TUnique>, IGetAllRepositoryAsync<TClass, TUnique>
        where TClass : class
    {
    }
}
