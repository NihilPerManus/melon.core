﻿using System.Threading.Tasks;

namespace Melon.Core.Repository.ReadOnly
{
    public interface IGetRepository<TClass, TUnique> where TClass : class
    {
        TClass Get(TUnique id);
    }

    public interface IGetRepositoryAsync<TClass, TUnique> where TClass : class
    {
        Task<TClass> GetAsync(TUnique id);
    }

}