﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Melon.Core.Repository.ReadOnly
{
    public interface IGetAllRepository<TClass, TUnique> where TClass : class
    {
        IEnumerable<TClass> GetAll();
    }

    public interface IGetAllRepositoryAsync<TClass, TUnique> where TClass : class
    {
        Task<IEnumerable<TClass>> GetAllAsync();
    }

}