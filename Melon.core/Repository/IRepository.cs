﻿using Melon.Core.Repository.ReadOnly;
using Melon.Core.Repository.WriteOnly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Melon.Core.Repository
{
    public interface IRepository<TClass, TUnique> : IReadOnlyRepository<TClass,TUnique>, IWriteOnlyRepository<TClass,TUnique>
        where TClass : class
    {
    }

    public interface IRepositoryAsync<TClass, TUnique> : IReadOnlyRepositoryAsync<TClass, TUnique>, IWriteOnlyRepositoryAsync<TClass,TUnique>
        where TClass : class
    {

    }

}
