﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Melon.Core.Repository.WriteOnly
{
    public interface IWriteOnlyRepository<TClass, TUnique> : 
        ICreateRepository<TClass>, 
        ICreateRangeRepository<TClass>,
        IUpdateRepository<TClass>,
        IDeleteRepository<TClass>
        where TClass : class
    {
    }

    public interface IWriteOnlyRepositoryAsync<TClass, TUnique> : 
        ICreateRepositoryAsync<TClass>, 
        ICreateRangeRepositoryAsync<TClass>, 
        IUpdateRepositoryAsync<TClass>, 
        IDeleteRepositoryAsync<TClass>
        where TClass : class
    {
    }
}
