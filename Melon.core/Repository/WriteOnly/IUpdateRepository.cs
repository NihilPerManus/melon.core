﻿using System.Threading.Tasks;

namespace Melon.Core.Repository.WriteOnly
{
    public interface IUpdateRepository<TClass> where TClass : class
    {
        void Update(TClass entity);
    }

    public interface IUpdateRepositoryAsync<TClass> where TClass : class
    {
        Task UpdateAsync(TClass entity);
    }
}