﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace Melon.Core.Repository.WriteOnly
{
    public interface ICreateRangeRepository<TClass> where TClass : class
    {
        void CreateRange(IEnumerable<TClass> entities);
    }

    public interface ICreateRangeRepositoryAsync<TClass> where TClass : class
    {
        Task CreateRangeAsync(IEnumerable<TClass> entities);
    }

}