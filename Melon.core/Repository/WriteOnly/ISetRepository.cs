﻿using System.Threading.Tasks;

namespace Melon.Core.Repository.WriteOnly
{
    public interface ICreateRepository<TClass> where TClass : class
    {
        void Create(TClass entity);
    }

    public interface ICreateRepositoryAsync<TClass> where TClass : class
    {
        Task CreateAsync(TClass entity);
    }

}