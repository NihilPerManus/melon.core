﻿using System.Threading.Tasks;

namespace Melon.Core.Repository.WriteOnly
{
    public interface IDeleteRepository<TClass> where TClass : class
    {
        void Delete(TClass id);
    }

    public interface IDeleteRepositoryAsync<TClass> where TClass : class
    {
        Task DeleteAsync(TClass id);
    }

}