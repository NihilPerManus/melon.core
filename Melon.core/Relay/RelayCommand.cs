﻿using System;
using System.Windows.Input;

//relay pattern
namespace Melon.Core.Relay
{
    //Use this as an ICommand class for separating execution logic from presentation logic. i.e a button should not be aware of the objects it is manipulating, it should only know that it needs to delegate a command 
    public class RelayCommand : IRelayCommand
    {
        private readonly Action<object> _action;

        public event EventHandler CanExecuteChanged = delegate { };

        public RelayCommand(Action<object> action)
        {
            _action = action;
        }

        public bool CanExecute(object parameter)
        {
            //you should write the logic where it's required before running the command I say. so return true.
            return true;
        }

        public void Execute(object parameter)
        {
            _action?.Invoke(parameter);
        }
    }

    public interface IRelayCommand : ICommand
    {

    }
}
