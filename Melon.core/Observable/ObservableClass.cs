﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Melon.Core.Observable
{
    //This class is used for classes that need their properties observed. Mostly used with WPF but has other uses.
    public abstract class ObservableClass : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected void NotifyPropertyChanged([CallerMemberName] string name = "") => PropertyChanged?.Invoke(this,new PropertyChangedEventArgs(name));        
    }
}
